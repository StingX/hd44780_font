/** \file main.cpp
  * \brief The main entry point
  * \author Devyatnikov A.
  * \date 30.12.2013
  * Project: hd44780_font
  * License: BSD (see LICENSE)
  */

#include "cfontedit.h"

#include <QApplication>
#include <QTranslator>
#include <QLibraryInfo>

int main(int argc, char *argv[])
{
	QApplication app(argc, argv);

	app.setOrganizationName(QLatin1String("Devyatnikov A.V."));
	app.setApplicationName(QLatin1String("hd44780_font"));
	app.setApplicationVersion(QLatin1String("1.0.0"));

	QString locale = QLocale::system().name();
	int idx = locale.indexOf(QLatin1Char('_'));
	if (idx != -1)
		locale = locale.left(idx);

	QTranslator translator;
	if (translator.load(QLatin1String(":/translations/combined_") + locale))
		app.installTranslator(&translator);

	CFontEdit w;
	return w.exec();
}
