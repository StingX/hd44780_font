/** \file cfont.cpp
  * \brief The implementation of CFont class
  * \author Devyatnikov A.
  * \date 30.12.2013
  * Project: hd44780_font
  * License: BSD (see LICENSE)
  */

#include "cfont.h"

#include <QImage>

CFont::CFont(QObject *parent)
	: QAbstractItemModel(parent)
	, m_font(8 * 256, '\0')
{
}

CFont::~CFont()
{
}

void CFont::setFont(const QByteArray &data)
{
	if (m_font != data)
	{
		beginResetModel();
		m_font = data;
		if (m_font.size() != 8 * 256)
			m_font.resize(8 * 256);
		m_imageCache.clear();
		endResetModel();
	}
}

QByteArray CFont::font()
{
	return m_font;
}

QImage CFont::characterImage(char c) const
{
	if (m_imageCache.contains(c))
		return m_imageCache[c];

	QImage img(5, 7, QImage::Format_Mono);

	for (int y = 0; y < 7; ++y)
	{
		*img.scanLine(y) = ~m_font.constData()[8 * (uchar)c + y] << 3;
	}
	m_imageCache.insert(c, img);
	return img;
}

QByteArray CFont::character(char c) const
{
	return QByteArray::fromRawData(m_font.constData() + 8 * (uchar)c, 8);
}

void CFont::setCharacter(char c, const QByteArray &data)
{
	if (data.size() == 8)
	{
		memcpy(m_font.data() + 8 * (uchar)c, data.constData(), 8);
		m_imageCache.remove(c);
		QModelIndex idx(index((uchar)c, 0, QModelIndex()));
		dataChanged(idx, idx);
	}
}

int CFont::columnCount(const QModelIndex &parent) const
{
	Q_UNUSED(parent);
	return 1;
}

QVariant CFont::data(const QModelIndex &index, int role) const
{
	if (index.row() < 0 || index.row() > 255)
		return QVariant();
	switch (role)
	{
	case Qt::DecorationRole:
		return characterImage((char)index.row()).scaled(5*3, 7*3);
	case Qt::DisplayRole:
		return QString().sprintf("0x%02X (%d)", index.row(), index.row());
	}
	return QVariant();
}

Qt::ItemFlags CFont::flags(const QModelIndex &/*index*/) const
{
	Qt::ItemFlags rv;

	rv |= Qt::ItemIsSelectable;
	rv |= Qt::ItemIsEnabled;

	return rv;
}

bool CFont::hasChildren(const QModelIndex &) const
{
	return false;
}

QVariant CFont::headerData(int section, Qt::Orientation orientation, int role) const
{
	if (orientation == Qt::Horizontal && section == 1 && role == Qt::DisplayRole)
		return tr("Character");
	return QVariant();
}

QModelIndex CFont::index(int row, int column, const QModelIndex &/*parent*/) const
{
	return createIndex(row, column);
}

QModelIndex CFont::parent(const QModelIndex &) const
{
	return QModelIndex();
}

int CFont::rowCount(const QModelIndex &parent) const
{
	if (!parent.isValid())
		return 256;
	return 0;
}
