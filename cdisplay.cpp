/** \file cdisplay.cpp
  * \brief The implementation of CDisplay class
  * \author Devyatnikov A.
  * \date 30.12.2013
  * Project: hd44780_font
  * License: BSD (see LICENSE)
  */

#include "cdisplay.h"
#include "cfont.h"

#include <QPainter>
#include <QEvent>
#include <QPaintEvent>


//Dimensions
static const float Scale = 5.0;		//pixels in mm
static const float Width = 3.55f;		//mm
static const float Height = 5.95f;		//mm
static const float CellWidth = 0.6f;	//mm
static const float CellHeight = 0.7f;	//mm
static const float PixelWidth = 0.55f;	//mm
static const float PixelHeight = 0.65f;	//mm
static const float BorderWidth = 5.1f;	//mm
static const float BorderHeight = 2.75f;//mm

CDisplay::CDisplay(QWidget *parent)
	: QWidget(parent)
	, m_luminosity(50)
	, m_columns(16)
	, m_rows(2)
{
}

void CDisplay::setFont(CFont *font)
{
	m_font = font;
	update();
}

void CDisplay::setText(const QByteArray &val)
{
	m_text = val;
	update();
}

void CDisplay::updateDisplay()
{
	update();
}

void CDisplay::setLuminosity(int value)
{
	m_luminosity = value;
	update();
}

void CDisplay::setSize(int columns, int rows)
{
	m_columns = columns;
	m_rows = rows;
	update();
}

static QRgb lerp(QRgb cBg, QRgb cFg, float alpha)
{
	return qRgb(qRed(cBg) + (qRed(cFg) - qRed(cBg)) * alpha,
				qGreen(cBg) + (qGreen(cFg) - qGreen(cBg)) * alpha,
				qBlue(cBg) + (qBlue(cFg) - qBlue(cBg)) * alpha);
}

static void drawChar(QPainter& p, int px, int py, const char* fontData, QRgb fgColor, QRgb bgColor)
{
	const int stepX = CellWidth * Scale;
	const int stepY = CellHeight * Scale;
	const int width = PixelWidth * Scale;
	const int height = PixelHeight * Scale;
	for (int y = 0; y < 7; ++y)
	{
		for (int x = 0; x < 5; ++x)
		{
			p.fillRect(px + x * stepX, py + y * stepY, width, height,
					   QColor((fontData[y] & (1 << (4 - x))) ? fgColor : bgColor));
		}
	}
}

void CDisplay::paintEvent(QPaintEvent * e)
{
	QPainter painter(this);

	// Подсчитаем цвета (закрашенного и незакрашенного пикселей)
	float luminosity = (m_luminosity * 2) / 100.0f;
	float alphaOn = luminosity;
	float alphaOff = 0.1f + (luminosity - 1.0f) * 2.0f;
	QRgb light = qRgb(150,255,30);	//green
	QRgb bgColor = lerp(light, 0, qBound(0.0f, alphaOff, 1.0f));//Transparent
	QRgb fgColor = lerp(light, 0, qBound(0.0f, alphaOn, 1.0f));	//Non transparent



	const int cx = BorderWidth * Scale;
	const int cy = BorderHeight * Scale;
	const int charWidth = Width * Scale;
	const int charHeight = Height * Scale;
	const int border = 10;
	painter.fillRect(rect(), QColor(qRgb(0,0,0)));
	painter.fillRect(border, border, charWidth * m_columns + cx * 2, charHeight * m_rows + cy * 2, QColor(light));

	const char *p = m_text.constData();
	const char *pstop = m_text.constEnd();

	for (int row = 0; row < m_rows; ++row)
	{
		for (int col = 0; col < m_columns; ++col)
		{
			if (p < pstop)
				drawChar(painter, border + cx + col * charWidth, border + cy + row * charHeight, m_font->character( *p++ ).constData(), fgColor, bgColor);
			else
				drawChar(painter, border + cx + col * charWidth, border + cy + row * charHeight, m_font->character( ' ' ).constData(), fgColor, bgColor);
		}
	}
	e->accept();
}
