/** \file cfontedit.h
  * \brief The declaration of CFontEdit class
  * \author Devyatnikov A.
  * \date 30.12.2013
  * Project: hd44780_font
  * License: BSD (see LICENSE)
  */

#ifndef CFONTEDIT_H
#define CFONTEDIT_H

#include <QDialog>
#include "cfont.h"
#include <QItemSelection>

namespace Ui {
class CFontEdit;
}

/** The main application dialg. Implements cfontedit.ui widget. */
class CFontEdit : public QDialog
{
	Q_OBJECT
	Q_DISABLE_COPY(CFontEdit)
public:
	explicit CFontEdit(QWidget *parent = 0);
	~CFontEdit();

private slots:
	/** Sets the new text for text output pane ui->binaryPreview */
	void updateBinaryView();

	/** Processes the user character selection from ui->fontView */
	void on_currentCharChanged(QModelIndex, QModelIndex);

	// Slots autocconnected in setupUi
	void on_pixelEdit_pixelsChanged(QByteArray);
	void on_displaySize_currentIndexChanged(int index);
	void on_previewText_textChanged(const QString &arg1);
	void on_fontView_activated(const QModelIndex &index);
	void on_viewHexOption_toggled(bool checked);
	void on_saveButton_clicked();
	void on_loadButton_clicked();

private:
	/** Sets the text displayed in the preview widget */
	void setPreviewText(const QString& text);
	/** Returns recently used directory that user used to save or load font to or from */
	QString lastFileLocation() const;
	/** Remebers recently used directory that user used to save or load font to or from */
	void saveLastFileLocation(const QString& dirName) const;

	Ui::CFontEdit *ui;				///< The UI object
	CFont m_font;					///< Font model
	char m_currentChar;				///< Current character index (=code) edited in the widgets
};

#endif // CFONTEDIT_H
