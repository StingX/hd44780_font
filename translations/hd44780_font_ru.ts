<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU">
<context>
    <name>CFont</name>
    <message>
        <location filename="../cfont.cpp" line="110"/>
        <source>Character</source>
        <translation>Символ</translation>
    </message>
</context>
<context>
    <name>CFontEdit</name>
    <message>
        <location filename="../cfontedit.ui" line="14"/>
        <source>HD44780 Character View/Edit</source>
        <translation>HD44780 Редактор/просмотровщик символов шрифта</translation>
    </message>
    <message>
        <location filename="../cfontedit.ui" line="32"/>
        <source>Font characters (double click to insert into preview text)</source>
        <translation>Символы шрифта (двойной щелчек для копирования в предварительный просмотр)</translation>
    </message>
    <message>
        <location filename="../cfontedit.ui" line="77"/>
        <source>Reads the font data from a file</source>
        <translation>Производит чтение даных шрифта из файла</translation>
    </message>
    <message>
        <location filename="../cfontedit.ui" line="80"/>
        <source>Load...</source>
        <translation>Загрузить...</translation>
    </message>
    <message>
        <location filename="../cfontedit.ui" line="87"/>
        <source>Saves a font data to a file</source>
        <translation>Записывает данные шрифта в файл</translation>
    </message>
    <message>
        <location filename="../cfontedit.ui" line="90"/>
        <source>Save...</source>
        <translation>Сохранить...</translation>
    </message>
    <message>
        <location filename="../cfontedit.ui" line="109"/>
        <source>Text preview</source>
        <translation>Предпросмотр текста</translation>
    </message>
    <message>
        <location filename="../cfontedit.ui" line="116"/>
        <source>Display size</source>
        <translation>Размер дисплея</translation>
    </message>
    <message>
        <location filename="../cfontedit.ui" line="123"/>
        <source>16x01</source>
        <translation>16x01</translation>
    </message>
    <message>
        <location filename="../cfontedit.ui" line="128"/>
        <source>16x02</source>
        <translation>16x02</translation>
    </message>
    <message>
        <location filename="../cfontedit.ui" line="133"/>
        <source>20x04</source>
        <translation>20x04</translation>
    </message>
    <message>
        <location filename="../cfontedit.ui" line="148"/>
        <source>Virtual &quot;contrast&quot;</source>
        <translation>&quot;Контрастность&quot; дисплея</translation>
    </message>
    <message>
        <location filename="../cfontedit.ui" line="166"/>
        <source>Preview text (C escapes accepted)</source>
        <translation>Текст предварительного просмотра (Разрешено экранирование в  C стиле)</translation>
    </message>
    <message>
        <location filename="../cfontedit.ui" line="169"/>
        <source>The quick brown fox jumps over the lazy dog!</source>
        <translation>The quick brown fox jumps over the lazy dog!</translation>
    </message>
    <message>
        <location filename="../cfontedit.ui" line="180"/>
        <source>Binary</source>
        <translation>Бинарный</translation>
    </message>
    <message>
        <location filename="../cfontedit.ui" line="187"/>
        <source>HEX</source>
        <translation>HEX</translation>
    </message>
    <message>
        <location filename="../cfontedit.cpp" line="411"/>
        <location filename="../cfontedit.cpp" line="418"/>
        <source>Save a font</source>
        <translation>Созранить шрифт</translation>
    </message>
    <message>
        <location filename="../cfontedit.cpp" line="411"/>
        <location filename="../cfontedit.cpp" line="429"/>
        <source>Binary file (*.bin);;Any file(*.*)</source>
        <translation>Бинарный файл (*.bin);;Все файлы (*.*)</translation>
    </message>
    <message>
        <location filename="../cfontedit.cpp" line="418"/>
        <source>Failed to save a font: %1</source>
        <translation>Ошибка сохранения шрифта: %1</translation>
    </message>
    <message>
        <location filename="../cfontedit.cpp" line="429"/>
        <location filename="../cfontedit.cpp" line="435"/>
        <location filename="../cfontedit.cpp" line="441"/>
        <location filename="../cfontedit.cpp" line="448"/>
        <source>Open a font</source>
        <translation>Открыть файл шрифта</translation>
    </message>
    <message>
        <location filename="../cfontedit.cpp" line="435"/>
        <source>Failed to read a font: %1</source>
        <translation>Ошибка чтения шрифта: %1</translation>
    </message>
    <message>
        <location filename="../cfontedit.cpp" line="441"/>
        <source>Invalid file size (expected exactly 2048 bytes)</source>
        <translation>Неверный размер файла (ожидается 2048 байт)</translation>
    </message>
    <message>
        <location filename="../cfontedit.cpp" line="448"/>
        <source>Read error: %1</source>
        <translation>Ошибка чтения: %1</translation>
    </message>
</context>
</TS>
