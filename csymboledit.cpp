/** \file csymboledit.cpp
  * \brief The implementation of CSymbolEdit class
  * \author Devyatnikov A.
  * \date 30.12.2013
  * Project: hd44780_font
  * License: BSD (see LICENSE)
  */


#include "csymboledit.h"

#include <QImage>
#include <QEvent>
#include <QPainter>
#include <QPaintEvent>

// Default symbol bytes
static const char defaultSymbolPixels[8] =
{ 0x10, 0x08, 0x04, 0x02, 0x01, 0x02, 0x04, 0x00};


CSymbolEdit::CSymbolEdit(QWidget *parent)
	: QWidget(parent)
	, m_pixels(defaultSymbolPixels, 8)
	, m_changed(false)
{
	setBackgroundRole(QPalette::Window);
}

QImage CSymbolEdit::renderToImage(int width, int height)
{
	QImage rv(width, height, QImage::Format_Mono);

	for (int y = 0; y < 7; ++y)
	{
		const uchar* src = (const uchar*)m_pixels.constData() + y;
		uchar* dst = rv.scanLine(y);
		memset(dst, 0, width / 8);

		for (int x = 3; x < 8; ++x)
		{
			int destX1 = (width / 8) * x;
			int destX2 = (width / 8) * (x + 1);
			int color = (src[x / 8] >> (x % 8)) & 1;
			for (int destX = destX1; destX < destX2; ++destX)
			{
				dst[destX / 8] |= color << (destX % 8);
			}
		}
	}
	return rv;
}

QByteArray CSymbolEdit::pixels()
{
	return m_pixels;
}

bool CSymbolEdit::isChanged() const
{
	return m_changed;
}

void CSymbolEdit::setPixels(const QByteArray &val)
{

	if (val != m_pixels)
	{
		m_pixels = val;
		if (m_pixels.size() != 8)
			m_pixels.resize(8);
		m_pixels.detach();
		setChanged(false);
		pixelsChanged(m_pixels);
		update();
	}
}

void CSymbolEdit::setChanged(bool bChanged)
{
	if (bChanged != m_changed)
	{
		m_changed = bChanged;
		emit changed(m_changed);
	}
}

void CSymbolEdit::paintEvent(QPaintEvent * e)
{
	QPainter p(this);

	int width = geometry().width() - 1;
	int height = geometry().height() - 1;

	//Draw background
	p.fillRect(0, 0, width, height, palette().window());
	p.setPen(palette().color(QPalette::Foreground));

	// Draw border lines
	for (int x = 0; x < 6; ++x)
		p.drawLine(width * x / 5, 0, width * x / 5, height);
	for (int y = 0; y < 8; ++y)
		p.drawLine(0, height * y / 7, width, height * y /7);

	//Draw pixels
	for (int y = 0; y < 7; ++y)
	{
		for (int x = 0; x < 5; ++x)
		{
			if (m_pixels.constData()[y] & (1 << (4 - x)))
				p.fillRect(width * x / 5 + 2, height * y / 7 + 2, width / 5 - 3, height / 7 - 3, palette().brush(QPalette::ButtonText));
		}
	}
	e->accept();
}

void CSymbolEdit::mousePressEvent(QMouseEvent * e)
{
	if (e->button() == Qt::LeftButton)
	{
		int width = geometry().width() - 1;
		int height = geometry().height() - 1;

		for (int y = 0; y < 7; ++y)
		{
			for (int x = 0; x < 5; ++x)
			{
				QRect rc(width * x / 5, height * y / 7, width / 5, height / 7);
				if (rc.contains(e->pos()))
				{
					setChanged(true);
					m_pixels.data()[y] ^= (1 << (4 - x));
					pixelsChanged(m_pixels);
					update();
					e->accept();
					return;
				}
			}
		}
	}
}

