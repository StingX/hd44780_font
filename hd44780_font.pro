QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

DEFINES += QT_NO_CAST_FROM_ASCII

SOURCES += main.cpp\
    cfontedit.cpp \
    csymboledit.cpp \
    cfont.cpp \
    cdisplay.cpp

HEADERS  += \
    cfontedit.h \
    csymboledit.h \
    cfont.h \
    cdisplay.h

FORMS    += \
    cfontedit.ui

greaterThan(QT_MAJOR_VERSION, 5)
{

LANGUAGES = ru

TRANSLATION_RESULTS =
qtPrepareTool(LCONVERT, lconvert)
for (lang, LANGUAGES) {
	join_qm_$${lang}.commands ''= $${LCONVERT} -o $$shadowed($$PWD/combined_$${lang}.qm) \
		$$PWD/translations/hd44780_font_$${lang}.qm \
		$$[QT_INSTALL_TRANSLATIONS]/qtbase_$${lang}.qm \
		$$[QT_INSTALL_TRANSLATIONS]/qt_$${lang}.qm
	join_qm_$${lang}.depends = $$PWD/translations/hd44780_font_$${lang}.qm
	QMAKE_EXTRA_TARGETS += join_qm_$${lang}
	PRE_TARGETDEPS += join_qm_$${lang}

	TRANSLATIONS ''= $$PWD/translations/hd44780_font_$${lang}.ts

	TRANSLATION_RESULTS ''= <file>combined_$${lang}.qm</file>
}

qrc.input = $$PWD/hd44780_font.qrc.in
qrc.output = hd44780_font.qrc
QMAKE_SUBSTITUTES = qrc

OTHER_FILES += \
	$$qrc.input \
	README \
	LICENSE

RESOURCES += \
	$$shadowed(hd44780_font.qrc)

}
