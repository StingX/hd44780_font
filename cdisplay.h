/** \file cdisplay.h
  * \brief The declaration of CDisplay class
  * \author Devyatnikov A.
  * \date 30.12.2013
  * Project: hd44780_font
  * License: BSD (see LICENSE)
  */

#ifndef CDISPLAY_H
#define CDISPLAY_H

#include <QWidget>

class CFont;

/** Implements a widget, that "mimics" HD44780-based black/green character
 * display
 */
class CDisplay : public QWidget
{
	Q_OBJECT
	Q_DISABLE_COPY(CDisplay)
public:
	explicit CDisplay(QWidget *parent = 0);

	/** Sets a current font data
	 *
	 * This class uses necessary signals from this font to automate redraw process.
	 */
	void setFont(CFont* font);

public slots:
	/** Sets a preview text displayed by the widget
	 *
	 * The caller must guarantee, that this object will alive all the time
	 * this class used.
	 */
	void setText(const QByteArray& val);

	/** Redraws a display */
	void updateDisplay();

	/** Sets a luminosity. This actually emulates the variable resistor, that
	 * adjusts HD44780 contrast
	 */
	void setLuminosity(int value);

	/** Sets the display size
	 *
	 * Most common sizes are:
	 * - 16x1
	 * - 16x2
	 * - 20x4
	 *
	 * \param columns The amount of columns
	 * \param rows The amount of rows
	 */
	void setSize(int columns, int rows);

protected:
	/** All the widget drawing goes here */
	virtual void paintEvent(QPaintEvent *);

private:
	CFont* m_font;					///< Current font used
	QByteArray m_text;				///< Preview text
	int m_luminosity;				///< The virtual "contrast" of the display
	int m_columns;					///< An amount of rows in the display
	int m_rows;						///< An amount of columns in the display
};

#endif // CDISPLAY_H
