/** \file csymboledit.h
  * \brief The declaration of CSymbolEdit class
  * \author Devyatnikov A.
  * \date 30.12.2013
  * Project: hd44780_font
  * License: BSD (see LICENSE)
  */

#ifndef CSYMBOLEDIT_H
#define CSYMBOLEDIT_H

#include <QWidget>

/** The widget used to edit one symbol
 */
class CSymbolEdit : public QWidget
{
	Q_OBJECT
	Q_DISABLE_COPY(CSymbolEdit)
public:
	explicit CSymbolEdit(QWidget *parent = 0);

	/** Renders current symbol to the image
	 * \param width - Desired width of the image
	 * \param height - Desired height of the image
	 * \return Monochrome image
	 */
	QImage renderToImage(int width = 8, int height = 8);

	/** Returns an internal pixel buffer
	 *
	 * \return The buffer containing exactly 8 bytes of the 8x8 monochrome image.
	 * \sa setPixels
	 */
	QByteArray pixels();

	/** Returns the flag, indicating that user has modified some pixel data */
	bool isChanged() const;
signals:
	/** Emited when the internal pixel buffer has been modified by the user
	 * or a call to #setPixels
	 *
	 * \param pixels Current pixel buffer
	 */
	void pixelsChanged(const QByteArray& pixels);

	/** Emited when the modification flag has changed due to user activity or
	 * after the #setChanged/#setPixels call.
	 *
	 * \param flag The new flag
	 */
	void changed(bool flag);

public slots:
	/** Sets the internal pixel buffer for the class
	 *
	 * If the supplied pixel buffer differs from the current one, the signal
	 * #pixelsChanged will be emited and flag #isChanged will be reset.
	 *
	 * \param val New pixel buffer. This buffer MUST contain exactly 8 bytes.
	 * \sa pixels
	 */
	void setPixels(const QByteArray& val);

	/** Sets the new value for #isChanged flag
	 * \param val The new value. If this value differs from the previous one, the
	 * #changed signal will be emited.
	 */
	void setChanged(bool val);


protected:
	/** All the drawing of the widget goes here */
	virtual void paintEvent(QPaintEvent*);

	/** Processes user mouse events */
	virtual void mousePressEvent(QMouseEvent*);

private:
	/** Helper function, which handles #isChanged flag modifications and
	 * optionally emits #changed signal.
	 */
	void _changed(bool);

	QByteArray m_pixels;		///< Actual pixels buffer (8x8 monochrome = 8 bytes)
	bool m_changed;				///< #isChanged flag value
};

#endif // CSYMBOLEDIT_H
