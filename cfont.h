/** \file cfont.h
  * \brief The declaration of CFont class
  * \author Devyatnikov A.
  * \date 30.12.2013
  * Project: hd44780_font
  * License: BSD (see LICENSE)
  */

#ifndef CFONT_H
#define CFONT_H

#include <QAbstractItemModel>

/** Font model used to store font pixels and provide text/preview to the font
 * character selection list
 */
class CFont: public QAbstractItemModel
{
	Q_OBJECT
	Q_DISABLE_COPY(CFont)
public:
	CFont(QObject* parent = 0);
	virtual ~CFont();

	/** Sets the current font pixels buffer
	 *
	 * \param data Monochrome images for 256 characters. This buffer must
	 * contain exactly 2048 bytes (256 symbols * 8 bytes per symbol)
	 * \sa font
	 */
	void setFont(const QByteArray& data);

	/** Returns current font buffer
	 * \return The 2048 bytes of the font data
	 * \sa setFont
	 */
	QByteArray font();

	/** Returns a preview of the character
	 * \param c Character code
	 */
	QImage characterImage(char c) const;

	/** Retruns a pixel data for the character
	 * \param c Character code
	 * \return Exactly 8 bytes (8x8 monochrome image)
	 */
	QByteArray character(char c) const;

	/** Sets a character pixel data
	 * \param c Character code
	 * \param data 8 bytes of pixel data (8x8 monochrome image)
	 */
	void setCharacter(char c, const QByteArray& data);

	// Overloads from QAbstractItemModel
	virtual int columnCount(const QModelIndex &parent) const;
	virtual QVariant data(const QModelIndex &index, int role) const;
	virtual Qt::ItemFlags flags(const QModelIndex &index) const;
	virtual bool hasChildren(const QModelIndex &parent) const;
	virtual QVariant headerData(int section, Qt::Orientation orientation, int role) const;
	virtual QModelIndex index(int row, int column, const QModelIndex &parent) const;
	virtual QModelIndex parent(const QModelIndex &child) const;
	virtual int rowCount(const QModelIndex &parent) const;

private:
	QByteArray m_font;							///< Current font (2048 bytes: 256 * (8x8 1bpp images))
	mutable QMap<char, QImage> m_imageCache;	///< Cache for the characters preview images.
};

#endif // CFONT_H
